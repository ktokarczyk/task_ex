﻿using ex7_task.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex7_task
{
    public partial class Form1 : Form
    {
        public BindingList<InventTable> InventTables { get; private set; }
        public BindingList<CommodityObject> CommodityObjects { get; private set; }

        public Form1()
        {
            InitializeComponent();

            InventTables = new BindingList<InventTable>()
            { 
                new InventTable($"{Guid.NewGuid()}", "Ssansa"),
                new InventTable($"{Guid.NewGuid()}", "Jkjk"),
                new InventTable($"{Guid.NewGuid()}", "Jjj")
            };
            CommodityObjects = new BindingList<CommodityObject>();
            
            dataGridView1.DataSource = InventTables;
            dataGridView2.DataSource = CommodityObjects;


           
        }

        /// <summary>
        /// Add button. Add new item to the InventTables.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            using (var addRecord = new FixRecord(InventTables))
            {
                addRecord.ShowDialog();
                if (addRecord.DialogResult == DialogResult.OK)
                {
                    richTextBox1.SelectionColor = Color.Green;
                    richTextBox1.AppendText($"One item was added to the source.\r");
                }
            }
        }

        /// <summary>
        /// Delete button. Delet specify item from InventTables.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                foreach (DataGridViewRow item in dataGridView1.SelectedRows)
                {
                    if (!item.IsNewRow)
                    {
                        dataGridView1.Rows.Remove(item);
                        i++;
                    }
                }

                if (i > 0)
                {
                    richTextBox1.SelectionColor = Color.Red;
                    richTextBox1.AppendText($"Item was deleted({i}).\r");
                }
            
            }
            catch (Exception ex)
            {
                richTextBox1.SelectionColor = Color.Red;
                richTextBox1.AppendText($"{ex.Message}\r");
            }

        }

        /// <summary>
        /// Fix button. Fix record in InventTalbes. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.SelectedRows.Count == 0)
                {
                    throw new Exception("Select a record first.");
                }

                if (dataGridView1.SelectedRows.Count > 1)
                {
                    throw new Exception("Can be fix only one record each time.");
                }

                if (dataGridView1.SelectedRows[0].IsNewRow)
                {
                    throw new Exception("Can select only existing record.");
                }

                var id = dataGridView1.SelectedRows[0].Cells[0].Value as string;

                using (var fixRecord = new EditRecord(InventTables, id))
                {
                    fixRecord.ShowDialog();
                    if (fixRecord.DialogResult == DialogResult.OK)
                    {
                        richTextBox1.SelectionColor = Color.Green;
                        richTextBox1.AppendText($"Item was fixed.\r");
                    }
                }
            }
            catch (Exception ex)
            {
                richTextBox1.SelectionColor = Color.Red;
                richTextBox1.AppendText($"{ex.Message}\r");
            }
        }

        /// <summary>
        /// Sunchronize button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var synchronizer = new Synchronization<InventTable, CommodityObject>();
                synchronizer.Compare = new Func<InventTable, CommodityObject, bool>(ComaprObjects);
                synchronizer.Remove = new Action<CommodityObject>(RemoveObject);
                synchronizer.Add = new Action<InventTable>(AddObject);
                synchronizer.Update = new Action<InventTable, CommodityObject>(UpdateObject);

                synchronizer.Synchronizer(InventTables, CommodityObjects);

                richTextBox1.SelectionColor = Color.Green;
                richTextBox1.AppendText($"Synchro is done\r");
            }
            catch (Exception ex)
            {
                richTextBox1.SelectionColor = Color.Red;
                richTextBox1.AppendText($"{ex.Message}\r");
            }

        }

        /// <summary>
        /// Compar objects grids objects.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destenation"></param>
        /// <returns></returns>
        private bool ComaprObjects(InventTable source, CommodityObject destenation)
        {
            bool isEqu = false;

            if (source.Id != destenation.Id)
            {
                return false;
            }

            if (source.Name == destenation.Name)
            {
                isEqu = true;
            }

            return isEqu;
        }

        /// <summary>
        /// Remove object from Commodity List
        /// </summary>
        /// <param name="destenation"></param>
        private void RemoveObject(CommodityObject destenation)
            => CommodityObjects.Remove(destenation);

        /// <summary>
        /// Add object to the Commodity list
        /// </summary>
        /// <param name="source"></param>
        private void AddObject(InventTable source)
        {
            var dest = new CommodityObject(source);
            CommodityObjects.Add(dest);
        }

        /// <summary>
        /// Update list
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destenation"></param>
        public void UpdateObject(InventTable source, CommodityObject destenation)
        {
            destenation.Name = source.Name;
            destenation.ModifiedDate = DateTime.Now;
        }
    }
}

﻿using ex7_task.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex7_task
{
    public partial class EditRecord : Form
    {
        private readonly BindingList<InventTable> _lnventTables;
        private readonly InventTable _editItem;
        private readonly int _bindingListIndex;

        public EditRecord(BindingList<InventTable> inventTables, string id)
        {
            InitializeComponent();

            _lnventTables = inventTables;

            _editItem = _lnventTables.SingleOrDefault(x => x.Id == id);

            _bindingListIndex = _lnventTables.IndexOf(_editItem);

            textBox1.Text = _editItem.Id;
            textBox2.Text = _editItem.Name;

        }

        /// <summary>
        /// Ok button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBox2.Text))
                {
                    throw new Exception("Pleas specify name of product.");
                }

                _editItem.Name = textBox2.Text;

                _lnventTables[_bindingListIndex] = new InventTable(_editItem);

                DialogResult = DialogResult.OK;
                Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

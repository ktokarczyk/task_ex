﻿using ex7_task.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ex7_task
{
    public partial class FixRecord : Form
    {
        private readonly BindingList<InventTable> _lnventTables;


        /// <summary>
        /// Add record with injecting our data source.
        /// </summary>
        /// <param name="inventTables"></param>
        public FixRecord(BindingList<InventTable> inventTables)
        {
            InitializeComponent();

            _lnventTables = inventTables;

            textBox1.Text = $"{Guid.NewGuid()}";
        }

        /// <summary>
        /// Create new item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBox2.Text))
                {
                    throw new Exception("Pleas specify name of product.");
                }
                 
                var newItem = new InventTable(textBox1.Text, textBox2.Text);

                _lnventTables.Add(newItem);
                DialogResult = DialogResult.OK;
                Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Cancel button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex7_task.Models
{
    public class CommodityObject : ILogisticData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }

        public CommodityObject(ILogisticData inventTable)
        {
            Id = inventTable.Id;
            Name = inventTable.Name;
            ModifiedDate = DateTime.Now;
        }

        public CommodityObject(string id, string name)
        {
            Id = id;
            Name = name;
            ModifiedDate = DateTime.Now;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex7_task.Models
{
    public interface ILogisticData
    {
        string Id { get; set; }
        string Name { get; set; }
        DateTime ModifiedDate { get; set; }
    }
}

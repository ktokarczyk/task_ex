﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex7_task.Models
{
    public class InventTable : ILogisticData
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }

        public InventTable(ILogisticData inventTable)
        {
            Id = inventTable.Id;
            Name = inventTable.Name;
            ModifiedDate = DateTime.Now;
        }

        public InventTable(string id, string name)
        {
            Id = id;
            Name = name;
            ModifiedDate = DateTime.Now;
        }

    }
}

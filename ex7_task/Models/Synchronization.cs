﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex7_task.Models
{
    /// <summary>
    /// For synchornize diffrence klass collections.
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TDestination"></typeparam>
    public class Synchronization<TSource, TDestination>
    {
        /// <summary>
        /// Compare source object with destination object.
        /// </summary>
        public Func<TSource, TDestination, bool> Compare { get; set; }
        /// <summary>
        /// Remove remove object from destenation collection.
        /// </summary>
        public Action<TDestination> Remove { get; set; }
        /// <summary>
        /// Add object to the destination collection.
        /// </summary>
        public Action<TSource> Add { get; set; }
        /// <summary>
        /// Update destination collection
        /// </summary>
        public Action<TSource, TDestination> Update { get; set; }

        /// <summary>
        /// Run synchronize on two collections.
        /// </summary>
        /// <param name="sourceItems"></param>
        /// <param name="destinationItems"></param>
        public void Synchronizer(ICollection<TSource> sourceItems, ICollection<TDestination> destinationItems)
        {
            RemoveItems(sourceItems, destinationItems);
            AddOrUpdateItems(sourceItems, destinationItems);
        }

        private void RemoveItems(ICollection<TSource> sourceCollection, ICollection<TDestination> destinationCollection)
        {
            foreach (var destinationItem in destinationCollection.ToArray())
            {
                var sourceItem = sourceCollection.FirstOrDefault(item => Compare(item, destinationItem));

                if (sourceItem == null)
                {
                    Remove(destinationItem);
                }
            }
        }

        private void AddOrUpdateItems(ICollection<TSource> sourceCollection, ICollection<TDestination> destinationCollection)
        {
            var destinationList = destinationCollection.ToList();
            foreach (var sourceItem in sourceCollection)
            {
                var destinationItem = destinationCollection.FirstOrDefault(item => Compare(sourceItem, item));

                if (destinationItem == null)
                {
                    Add(sourceItem);
                }
                else
                {
                    Update(sourceItem, destinationItem);
                }
            }
        }
    }
}
